from rest_framework import status

from web.tests.factories import SiteFactory


def test_login(api_client):
    response = api_client.post("/api/login/", data={"email": "asdfdadfadf@adf.ru", "password": "incorrect_password"})
    assert response.status_code == status.HTTP_403_FORBIDDEN, response.json()


def test_sites(api_client, user, auth_headers):
    site_from_another_user = SiteFactory()  # site for another user
    my_site = SiteFactory(user=user)

    response = api_client.get("/api/sites/", **auth_headers)

    # check status code
    assert response.status_code == status.HTTP_200_OK
    data = response.json()

    # check filtering by user
    assert len(data) == 1
    site_ids = [site["id"] for site in data]
    assert my_site.id in site_ids
    assert site_from_another_user.id not in site_ids
