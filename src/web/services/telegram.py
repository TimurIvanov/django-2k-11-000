import telebot
from django.conf import settings
from telebot.types import Message

from web.models import TelegramHash, User

bot = telebot.TeleBot(settings.TELEGRAM_BOT_TOKEN, parse_mode=None)


@bot.message_handler(commands=["start"])
def send_welcome(message: Message):
    text = message.text
    try:
        hash = text.split(" ")[1]
    except IndexError:
        bot.reply_to(message, f"Для подключения аккаунта на сайте перейдите по ссылке в профиле")
        return
    telegram_hash = TelegramHash.objects.get(hash=hash)
    telegram_hash.user.telegram_user_id = message.from_user.id
    telegram_hash.user.save()
    bot.reply_to(message, f"Вы вошли как {telegram_hash.user.email}")
    telegram_hash.delete()


@bot.message_handler()
def process_all_messages(message):
    bot.reply_to(message, "Вы что-то написали")


def send_message(user: User, message_text: str):
    bot.send_message(user.telegram_user_id, message_text)


def start_bot():
    bot.infinity_polling()
