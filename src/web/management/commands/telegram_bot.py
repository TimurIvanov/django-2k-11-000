from django.core.management import BaseCommand

from web.services.telegram import start_bot


class Command(BaseCommand):
    help = "Start Telegram bot"

    def handle(self, *args, **options):
        print("start telegram bot")
        start_bot()
