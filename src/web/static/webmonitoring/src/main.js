import { createApp } from 'vue'
import SiteHistory from "@/components/SiteHistory";

createApp(SiteHistory).mount('#site_history')
