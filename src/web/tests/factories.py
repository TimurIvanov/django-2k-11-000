import factory

from web.models import User, SiteHistory, Site


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    email = factory.Faker("email")


class SiteFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Site

    url = "http://example.local"
    name = factory.Faker("name")
    user = factory.SubFactory(UserFactory)


class SiteHistoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SiteHistory

    site = factory.SubFactory(SiteFactory)
    status_code = 200
